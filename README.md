# pocket-gopher

Ansible role to deploy Boundary Enterprise

## Example site.yml

```
- hosts: controllers
  gather_facts: yes
  become: yes

  vars:
    boundary_controller: true

    # Set boundary_local_archive_path to Boundary Ent zip file
    # when remote hosts cannot download the file from the Internet
    boundary_local_archive_path: /local/path/to/boundary_0.14.3+ent_linux_amd64.zip
    boundary_tls_cert_file: /local/path/to/cert.pem
    boundary_tls_key_file: /local/path/to/cert-key.pem
    boundary_license_file: /local/path/to/boundary.hclic

    # DB Connection Settings
    boundary_db_host: dbhostname
    boundary_db_username: dbusername
    boundary_db_password: dbpassword

  roles:
    - ansible-role-boundary
```
